#include <IRremote.h>
#include <SoftwareServo.h>

SoftwareServo servo;
int poz=0;

int receiverpin=1;
IRrecv irrecv(1); 
decode_results results;

void setup() {
  irrecv.enableIRIn();
  servo.attach(0);
  pinMode(A2,INPUT);//A1
}

void loop() {
  if (irrecv.decode(&results)) // sprawdza, czy otrzymano sygnał IR
   {
    unsigned long odczyt=results.value;
    irrecv.resume();

     if(odczyt==1759943551){//up
      go(1);
     }else{
      if(odczyt==574358183){//down
       go(0);
      }
     }
   }

  if(analogRead(A2)<=140){//82
    go(1);
    //delay(500);
  }else{
    if(analogRead(A2)>=307){
      go(0);
      //delay(500);
    }
  }
delay(100);
}

void go(bool kierunek){
  if((poz==0)&&(kierunek==0)||(poz==180)&&(kierunek==1)){
    return;
  }
  int i=1, fin=180;//poz=1
  if(poz==180){
    i=-1;
    fin=0;
  }
  for(;poz!=fin;poz+=i){
    servo.write(poz);
    delay(10);
    SoftwareServo::refresh();
  }
  delay(500);
  return;
}

